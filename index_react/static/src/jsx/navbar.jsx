const {
  Tabs,
  Tab,
  Typography,
  makeStyles,
  Box,
  Icon,
  Grid,
  Paper,
  Button,
  GridList,
  GridListTile,
  Badge
} = MaterialUI;

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
  },
  tabpanel: {
    margin: 0
  },
  tab: {
    width: '3em',
    borderRight: `1px solid ${theme.palette.divider}`,
  },
}));

function VerticalTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
    ReactDOM.render(
      <Isi idx={newValue} variant={1} />,
      document.getElementById('isi')
    );
  };

  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        <Tab icon={<Icon>pets</Icon>} className={classes.tab} label="Hewan" {...a11yProps(0)} />
        <Tab icon={<Icon>contact_support</Icon>} className={classes.tab} label="Perawatan" {...a11yProps(1)} />
        <Tab icon={<Icon>local_hospital</Icon>} className={classes.tab} label="Sakit" {...a11yProps(2)} />
        <Tab icon={<Icon>fastfood</Icon>} className={classes.tab} label="Pakan" {...a11yProps(3)} />
        <Tab icon={<Icon>access_time</Icon>} className={classes.tab} label="Waktu Makan" {...a11yProps(4)} />
      </Tabs>
    </div>
  );
}

const useStylesHeader = makeStyles(theme => ({

}));

function Header() {
  const classes = useStylesHome();
  return (
    <Grid item alignItems="center" justify="flex-end" container spacing={3}>
      <Grid id="username" item>
        <Typography variant={'h5'} style={{ color: "#ffffff" }}><b>{getCookie("username")}</b></Typography>
      </Grid>
      {/* <Grid item>
        <Badge badgeContent={1} color="secondary">
          <Icon>notifications</Icon>
        </Badge>
      </Grid> */}
      <Grid item>
        <Button onClick={() =>{
          window.document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "login"; 
        }}><Typography variant={'h5'} style={{ color: "#ffffff" }}>Logout</Typography></Button>
      </Grid>
    </Grid>
  );
}

function Isi(props) {
  const idx = props.idx;
  const variant = props.variant || 1;
  if (idx == 0) {
    if (variant == 1) {
      return <DataHewan />
    } else {
      return <AddDataHewan />;
    }
  } else if (idx == 1) {
    if (variant == 1) {
      return <Perawatan />
    } else {
      return <AddPerawatan />;
    }
  } else if (idx == 2) {
    if (variant == 1) {
      return <Sakit />;
    } else {
      return <AddSakit />;
    }
  } else if (idx == 3) {
    if (variant == 1) {
      return <DataPakan />;
    } else {
      return <AddDataPakan />;
    }
  } else if (idx == 4) {
    if (variant == 1) {
      return <WaktuMakan />;
    } else {
      return <AddWaktuMakan />;
    }
  } else {
    return (
      <React.Fragment>
        <Grid item>
          <Typography variant={'h2'}>TODO : {props.idx}</Typography>
        </Grid>
      </React.Fragment>
    );
  }
}

window.Isi = Isi;

const useStylesHome = makeStyles(theme => ({
  div: {
    flexGrow: 1,
    backgroundColor: '#87db63',
    display: 'flex',
    height: '100vh',
    padding: 0,
    margin: 0
  },
  grid: {
    padding: 0,
    margin: 0,
  }
}));


function Home() {
  const classes = useStylesHome();
  return (
    <div className={classes.div}>
      <Grid className={classes.grid} container direction="row" justify="center" alignItems="center" spacing={5}>
        <Grid item>
          <VerticalTabs />
        </Grid>
        <Grid item>
          <Grid className={classes.grid} container direction="column" justify="center" alignItems="center" spacing={3}>
            <Header />
            <div id="isi"></div>
          </Grid>
        </Grid>
      </Grid>

    </div>
  );
}

ReactDOM.render(
  <Home />,
  document.getElementById('root')
);


// Default page
ReactDOM.render(
  <Isi idx={0} variant={1} />,
  document.getElementById('isi')
);

if(getCookie("username") == "")
{
  window.location.href = "login";
}
