const {
    Typography,
    makeStyles,
    Grid,
    Paper,
    Button,
    GridList,
    GridListTile,
    TextField,
    Modal,
    Fade,
    Backdrop
} = MaterialUI;

function WaktuMakanListItem({ nama, waktu, cols, ...other }) {
    return (
        <React.Fragment>
            <GridListTile cols={cols || 1} {...other} style={{ width: '30%' }}>
                <Typography>{nama}</Typography>
            </GridListTile>
            <GridListTile cols={cols || 1} {...other} style={{ width: '70%' }}>
                <Typography>{waktu}</Typography>
            </GridListTile>
        </React.Fragment>
    );
}


const useStylesWaktuMakan = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        margin: 0,
        width: '500px',
        height: '300px',
    },
    grid: {
        padding: 0,
        margin: 0,
    },
    gridList: {
        width: '500px',
        height: '300px'
    }
}));

function WaktuMakan() {
    const classes = useStylesWaktuMakan();
    return (
        <React.Fragment>
            <Grid item container className={classes.grid} direction="column" spacing={2}>
                <Grid className={classes.grid} item justify="flex-start" container>
                    <Typography variant={'h3'} style={{ color: "#ffffff" }}>Waktu Makan</Typography>
                </Grid>
                <Grid className={classes.grid} item justify="center" container>
                    <Paper className={classes.paper}>
                        <GridList id="listMakan" cellHeight="auto" className={classes.gridList} cols={2} />
                    </Paper>
                </Grid>
                <Grid item justify="flex-end" container spacing={2}>
                    <Grid item>
                        <Button variant="contained" className={classes.button} onClick={() => {
                            ReactDOM.render(
                                <Isi idx={4} variant={2} />,
                                document.getElementById('isi')
                            );
                        }}>Add More</Button>
                    </Grid>
                </Grid>
            </Grid>
            {
                executeKw(uid, 'index_react.waktu_makan', 'search_read', [], {}, (r, s, j) => {
                    if (r[0].length > 0) {
                        const listMakan = [];
                        const groupNama = groupBy(r[0], 'nama_satwa');
                        groupNama.forEach(n => {
                            var temp = [];
                            n.forEach(w => {
                                temp.push(w.waktu_makan);
                            });
                            temp = temp.join(", ");
                            listMakan.push(<WaktuMakanListItem nama={n[0].nama_satwa} waktu={temp} cols={1} />);
                        });
                        ReactDOM.render(
                            listMakan,
                            document.getElementById('listMakan')
                        );
                    }
                }, typicalErrorCB)
            }
        </React.Fragment>
    );
}

const useStylesAddWaktuMakan = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    paperModal: {
        padding: theme.spacing(2),
        margin: 0,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 0,
        width: '500px',
        height: '300px',
    },
    grid: {
        padding: 0,
        margin: 0,
    }
}));

function submitWaktuMakan(setOpen, setText) {
    var namaHewan = window.document.getElementById("namaHewan").value;
    var waktu = window.document.getElementById("waktu").value;
    var keterangan = window.document.getElementById("keterangan").value;
    // Validasi
    var regexp = /^([0-1]\d|2[0-3]):([0-5]\d)$/i;
    var regex = new RegExp(regexp);
    if (regex.exec(waktu) && namaHewan != "") {
        // Valid
        var createParam = {
            'nama_satwa': namaHewan,
            'waktu_makan': waktu
        }
        executeKw(2, 'index_react.waktu_makan', 'create', [createParam], {}, (r, s, j) => {
            ReactDOM.render(
                <Isi idx={4} variant={1} />,
                document.getElementById('isi')
            );
        }, (j, s, e) => {
            setText(e);
            setOpen(true);
        });
    } else {
        var errText = "";
        if (!regex.exec(waktu)) {
            errText += "Waktu salah (format = HH:MM)";
        }
        if (namaHewan == "") {
            errText += "Nama Hewan tidak boleh kosong";
        }
        setText(errText);
        setOpen(true);
    }
}

function AddWaktuMakan() {
    const classes = useStylesAddWaktuMakan();
    const [openModal, setOpen] = React.useState(false);
    const [modalText, setText] = React.useState("");

    const handleClose = () => {
        setOpen(false);
    };
    return (
        <React.Fragment>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openModal}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={openModal}>
                    <Paper className={classes.paperModal}>
                        <Typography>{modalText}</Typography>
                    </Paper>
                </Fade>
            </Modal>
            <Grid item container className={classes.grid} direction="column" spacing={2}>
                <Grid className={classes.grid} item justify="flex-start" container>
                    <Typography variant={'h3'} style={{ color: "#ffffff" }}>Waktu Makan</Typography>
                </Grid>
                <Grid className={classes.grid} item justify="center" container>
                    <Paper className={classes.paper}>
                        <Grid className={classes.grid} container justify="center" alignItems="flex-end" spacing={2}>
                            <Grid item xs={4}>
                                <Typography>Nama Hewan</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="namaHewan" label="Nama Hewan" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Waktu</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="waktu" label="Waktu" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Keterangan</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="keterangan" label="Keterangan" autoComplete="off" />
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                <Grid className={classes.grid} item justify="flex-end" container spacing={2}>
                    <Grid item>
                        <Button variant="contained" className={classes.button} onClick={() => {
                            ReactDOM.render(
                                <Isi idx={4} variant={1} />,
                                document.getElementById('isi')
                            );
                        }}>Cancel</Button>
                    </Grid>
                    <Grid item>
                        <Button variant="contained" className={classes.button} onClick={() => {
                            submitWaktuMakan(setOpen, setText);
                        }}>Submit</Button>
                    </Grid>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}

window.WaktuMakan = WaktuMakan;
window.AddWaktuMakan = AddWaktuMakan;