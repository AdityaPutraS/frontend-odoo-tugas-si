const {
    Typography,
    makeStyles,
    Grid,
    Paper,
    Button,
    GridList,
    GridListTile,
    TextField,
    Modal,
    Fade,
    Backdrop
} = MaterialUI;

var cntSakit = 0;

function SakitListItem({ nama, penyakit, cols, ...other }) {
    return (
        <React.Fragment>
            <GridListTile key={cntSakit++} cols={cols || 1} {...other} style={{width: '100%'}}>
                <Typography><b>{nama}</b></Typography>
            </GridListTile>
            {
                penyakit.map(p => (
                    <GridListTile key={cntSakit++} cols={cols || 1} {...other} style={{width: '100%'}}>
                        <Typography>- {p}</Typography>
                    </GridListTile>
                ))
            }
        </React.Fragment>
    );
}


const useStylesSakit = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        margin: 0,
        width: '500px',
        height: '300px',
    },
    grid: {
        padding: 0,
        margin: 0,
    },
    gridList: {
        width: '500px',
        height: '300px'
    }
}));

function Sakit() {
    const classes = useStylesSakit();
    return (
        <React.Fragment>
        <Grid item container className={classes.grid} direction="column" spacing={2}>
            <Grid className={classes.grid} item justify="flex-start" container>
                <Typography variant={'h3'} style={{ color: "#ffffff" }}>Hewan Sakit</Typography>
            </Grid>
            <Grid className={classes.grid} item justify="center" container>
                <Paper className={classes.paper}>
                    <GridList id="listSakit" cellHeight={25} className={classes.gridList} cols={1}/>
                </Paper>
            </Grid>
            <Grid item justify="flex-end" container spacing={2}>
                <Grid item>
                    <Button variant="contained" className={classes.button} onClick={() => {
                        ReactDOM.render(
                            <Isi idx={2} variant={2} />,
                            document.getElementById('isi')
                        );
                    }}>Add More</Button>
                </Grid>
            </Grid>
        </Grid>
        {
            executeKw(uid, 'index_react.rekam_medis', 'search_read', [], {}, (r, s, j) => {
                if (r[0].length > 0) {
                    const listSakit = [];
                    const groupNama = groupBy(r[0], 'nama_satwa');
                    groupNama.forEach(n => {
                        const temp = [];
                        n.forEach(p => {
                            temp.push(p.penyakit);
                        })
                        listSakit.push(<SakitListItem nama={n[0].nama_satwa} penyakit={temp}/>);
                    });
                    console.log(listSakit);
                    ReactDOM.render(
                        listSakit,
                        document.getElementById('listSakit')
                    );
                }
            }, typicalErrorCB)
        }
        </React.Fragment>
    );
}

const useStylesAddSakit = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    paperModal: {
        padding: theme.spacing(2),
        margin: 0,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 0,
        width: '500px',
        height: '400px',
    },
    grid: {
        padding: 0,
        margin: 0,
    }
}));

function submitSakit(setOpen, setText) {
    var namaSatwa = window.document.getElementById("namaSatwa").value;
    var penyakit = window.document.getElementById("penyakit").value;
    var tanggal = window.document.getElementById("tanggalSakit").value;
    var dokter = window.document.getElementById("dokter").value;
    var tempatRawat = window.document.getElementById("tempatRawat").value;
    // Validasi
    if (namaSatwa != "" && penyakit != "" && Date.parse(tanggal) && dokter != "" && tempatRawat != "") {
        // Valid
        var createParam = { 
            'tanggal_rekam_medis': tanggal, 
            'penyakit': penyakit, 
            'tempat_rawat': tempatRawat, 
            'dokter': dokter,
            'nama_satwa': namaSatwa
        }
        executeKw(2, 'index_react.rekam_medis', 'create', [createParam], {}, (r, s, j) => {
            ReactDOM.render(
                <Isi idx={2} variant={1} />,
                document.getElementById('isi')
            );
        }, (j, s, e) => {
            setText(e);
            setOpen(true);
        });
    } else {
        var errText = "";
        if (!Date.parse(tanggal)) {
            errText += "Tanggal salah (format = YYYY-MM-DD, ";
        }
        if (namaSatwa == "") {
            errText += "Isi nama satwa, ";
        }
        if (penyakit == "") {
            errText += "Isi nama penyakit, ";
        }
        if (dokter == "") {
            errText += "Isi nama dokter, ";
        }
        if (tempatRawat == "") {
            errText += "Isi nama tempat rawat, ";
        }
        setText(errText);
        setOpen(true);
    }
}

function AddSakit() {
    const classes = useStylesAddSakit();
    const [openModal, setOpen] = React.useState(false);
    const [modalText, setText] = React.useState("");

    const handleClose = () => {
        setOpen(false);
    };
    return (
        <React.Fragment>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openModal}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={openModal}>
                    <Paper className={classes.paperModal}>
                        <Typography>{modalText}</Typography>
                    </Paper>
                </Fade>
            </Modal>
            <Grid item container className={classes.grid} direction="column" spacing={2}>
                <Grid className={classes.grid} item justify="flex-start" container>
                    <Typography variant={'h3'} style={{ color: "#ffffff" }}>Data Hewan</Typography>
                </Grid>
                <Grid className={classes.grid} item justify="center" container>
                    <Paper className={classes.paper}>
                        <Grid className={classes.grid} container justify="center" alignItems="flex-end" spacing={2}>
                            <Grid item xs={4}>
                                <Typography>Nama Satwa</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="namaSatwa" label="Nama Satwa" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Penyakit</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="penyakit" label="Penyakit" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Tanggal Sakit</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="tanggalSakit" label="Tanggal Sakit" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Dokter</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="dokter" label="Dokter" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Tempat Rawat</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="tempatRawat" label="Tempat Rawat" autoComplete="off" />
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                <Grid className={classes.grid} item justify="flex-end" container spacing={2}>
                    <Grid item>
                        <Button variant="contained" className={classes.button} onClick={() => {
                            ReactDOM.render(
                                <Isi idx={2} variant={1} />,
                                document.getElementById('isi')
                            );
                        }}>Cancel</Button>
                    </Grid>
                    <Grid item>
                        <Button variant="contained" className={classes.button} onClick={() =>{
                            submitSakit(setOpen, setText);
                        }}>Submit</Button>
                    </Grid>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}

window.Sakit = Sakit;
window.AddSakit = AddSakit;