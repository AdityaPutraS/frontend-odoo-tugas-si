const {
    Typography,
    makeStyles,
    Grid,
    Paper,
    Button,
    GridList,
    GridListTile,
    TextField,
    Modal,
    Fade,
    Backdrop
} = MaterialUI;

function PakanListItem({ idPakan, nama, jumlah, cols, ...other }) {
    return (
        <React.Fragment>
            <GridListTile cols={cols || 1} {...other} style={{ width: '10%' }}>
                <Typography>{idPakan}</Typography>
            </GridListTile>
            <GridListTile cols={cols || 1} {...other} style={{ width: '45%' }}>
                <Typography>{nama}</Typography>
            </GridListTile>
            <GridListTile cols={cols || 1} {...other} style={{ width: '45%' }}>
                <Typography>{jumlah}</Typography>
            </GridListTile>
        </React.Fragment>
    );
}


const useStylesDataPakan = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        margin: 0,
        width: '500px',
        height: '300px',
    },
    grid: {
        padding: 0,
        margin: 0,
    },
    gridList: {
        width: '500px',
        height: '300px'
    }
}));

function DataPakan() {
    const classes = useStylesDataPakan();
    return (
        <React.Fragment>
            <Grid item container className={classes.grid} direction="column" spacing={2}>
                <Grid className={classes.grid} item justify="flex-start" container>
                    <Typography variant={'h3'} style={{ color: "#ffffff" }}>Data Pakan</Typography>
                </Grid>
                <Grid className={classes.grid} item justify="center" container>
                    <Paper className={classes.paper}>
                        <GridList id="listPakan" cellHeight={25} className={classes.gridList} cols={2} />
                    </Paper>
                </Grid>
                <Grid item justify="flex-end" container spacing={2}>
                    <Grid item>
                        <Button variant="contained" className={classes.button} onClick={() => {
                            ReactDOM.render(
                                <Isi idx={3} variant={2} />,
                                document.getElementById('isi')
                            );
                        }}>Add More</Button>
                    </Grid>
                </Grid>
            </Grid>
            {
                executeKw(uid, 'index_react.pakan', 'search_read', [], {}, (r, s, j) => {
                    if (r[0].length > 0) {
                        const listPakan = [];
                        r[0].forEach(pakan => {
                            listPakan.push(<PakanListItem idPakan={pakan.id} nama={pakan.nama_pakan} jumlah={pakan.stok_pakan} cols={1} />);
                        });
                        ReactDOM.render(
                            listPakan,
                            document.getElementById('listPakan')
                        );
                    }
                }, typicalErrorCB)
            }
        </React.Fragment>
    );
}

const useStylesAddDataPakan = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    paperModal: {
        padding: theme.spacing(2),
        margin: 0,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 0,
        width: '500px',
        height: '300px',
    },
    grid: {
        padding: 0,
        margin: 0,
    }
}));

function submitDataPakan(setOpen, setText) {
    var namaPakan = window.document.getElementById("namaPakan").value;
    var jumlah = parseInt(window.document.getElementById("jumlah").value);
    var keterangan = window.document.getElementById("keterangan").value;

    //Validasi
    if (namaPakan != "" && jumlah != NaN) {
        // Valid
        // Cek apakah sudah ada di database
        executeKw(uid, 'index_react.pakan', 'search_read', [[['nama_pakan', '=', namaPakan]]], {}, (rS, sS, jS) => {
            var ada = rS[0].length > 0;
            if (ada) {
                var id = rS[0][0].id;
                if (jumlah > 0) {
                    var updateParam = {
                        'stok_pakan': jumlah
                    };
                    executeKw(2, 'index_react.pakan', 'write', [[id], updateParam], {}, (r, s, j) => {
                        ReactDOM.render(
                            <Isi idx={3} variant={1} />,
                            document.getElementById('isi')
                        );
                    }, (j, s, e) => {
                        setText(e);
                        setOpen(true);
                    });
                } else {
                    executeKw(2, 'index_react.pakan', 'unlink', [[id]], {}, (r, s, j) => {
                        ReactDOM.render(
                            <Isi idx={3} variant={1} />,
                            document.getElementById('isi')
                        );
                    }, (j, s, e) => {
                        setText(e);
                        setOpen(true);
                    });
                }
            } else {
                var createParam = {
                    'nama_pakan': namaPakan,
                    'stok_pakan': jumlah
                };
                executeKw(2, 'index_react.pakan', 'create', [createParam], {}, (r, s, j) => {
                    ReactDOM.render(
                        <Isi idx={3} variant={1} />,
                        document.getElementById('isi')
                    );
                }, (j, s, e) => {
                    setText(e);
                    setOpen(true);
                });
            }
        }, typicalErrorCB)
    } else {
        var errText = "";
        if (namaPakan == "") {
            errText += "Nama pakan tidak boleh kosong";
        }
        if (!jumlah) {
            errText += "Jumlah salah";
        }
        setText(errText);
        setOpen(true);
    }
}

function AddDataPakan() {
    const classes = useStylesAddDataPakan();
    const [openModal, setOpen] = React.useState(false);
    const [modalText, setText] = React.useState("");

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <React.Fragment>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openModal}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={openModal}>
                    <Paper className={classes.paperModal}>
                        <Typography>{modalText}</Typography>
                    </Paper>
                </Fade>
            </Modal>
            <Grid item container className={classes.grid} direction="column" spacing={2}>
                <Grid className={classes.grid} item justify="flex-start" container>
                    <Typography variant={'h3'} style={{ color: "#ffffff" }}>Data Pakan</Typography>
                </Grid>
                <Grid className={classes.grid} item justify="center" container>
                    <Paper className={classes.paper}>
                        <Grid className={classes.grid} container justify="center" alignItems="flex-end" spacing={2}>
                            <Grid item xs={4}>
                                <Typography>Nama Pakan</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="namaPakan" label="Nama Pakan" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Jumlah</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="jumlah" label="Jumlah" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Keterangan</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="keterangan" label="Keterangan" autoComplete="off" />
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                <Grid className={classes.grid} item justify="flex-end" container spacing={2}>
                    <Grid item>
                        <Button variant="contained" className={classes.button} onClick={() => {
                            ReactDOM.render(
                                <Isi idx={3} variant={1} />,
                                document.getElementById('isi')
                            );
                        }}>Cancel</Button>
                    </Grid>
                    <Grid item>
                        <Button variant="contained" className={classes.button} onClick={() => {
                            submitDataPakan(setOpen, setText);    
                        }}>Submit</Button>
                    </Grid>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}

window.DataPakan = DataPakan;
window.AddDataPakan = AddDataPakan;