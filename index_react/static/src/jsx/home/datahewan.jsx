const {
    Typography,
    makeStyles,
    Grid,
    Paper,
    Button,
    GridList,
    GridListTile,
    TextField,
    Modal,
    Fade,
    Backdrop
} = MaterialUI;

var cntHewanList = 0;

function HewanListItem({ idSatwa, namaSatwa, jenisSatwa, cols, ...other }) {
    return (
        <React.Fragment>
            <GridListTile key={cntHewanList++} cols={cols || 1} {...other} style={{width: '10%'}}>
                <Typography>{idSatwa}</Typography>
            </GridListTile>
            <GridListTile key={cntHewanList++} cols={cols || 1} {...other} style={{width: '45%'}}>
                <Typography>{namaSatwa}</Typography>
            </GridListTile>
            <GridListTile key={cntHewanList++} cols={cols || 1} {...other} style={{width: '45%'}}>
                <Typography>{jenisSatwa}</Typography>
            </GridListTile>
        </React.Fragment>
    );
}


const useStylesDataHewan = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        margin: 0,
        width: '500px',
        height: '300px',
    },
    grid: {
        padding: 0,
        margin: 0,
    },
    gridList: {
        width: '500px',
        height: '300px'
    }
}));

function DataHewan() {
    const classes = useStylesDataHewan();
    return (
        <React.Fragment>
        <Grid item container className={classes.grid} direction="column" spacing={2}>
            <Grid className={classes.grid} item justify="flex-start" container>
                <Typography variant={'h3'} style={{ color: "#ffffff" }}>Data Hewan</Typography>
            </Grid>
            <Grid className={classes.grid} item justify="center" container>
                <Paper className={classes.paper}>
                    <GridList id="listSatwa" cellHeight={25} className={classes.gridList} cols={3}/>
                </Paper>
            </Grid>
            <Grid item justify="flex-end" container spacing={2}>
                <Grid item>
                    <Button variant="contained" className={classes.button} onClick={() => {
                        ReactDOM.render(
                            <Isi idx={0} variant={2} />,
                            document.getElementById('isi')
                        );
                    }}>Add More</Button>
                </Grid>
            </Grid>
        </Grid>
        {
            executeKw(uid, 'index_react.satwa', 'search_read', [], {}, (r, s, j) => {
                if (r[0].length > 0) {
                    const listHewan = [];
                    r[0].forEach(satwa => {
                        listHewan.push(<HewanListItem idSatwa={satwa.id} namaSatwa={satwa.nama_satwa} jenisSatwa={satwa.jenis_satwa} />);
                    });
                    {/* console.log(listHewan); */}
                    ReactDOM.render(
                        listHewan,
                        document.getElementById('listSatwa')
                    );
                }
            }, typicalErrorCB)
        }
        </React.Fragment>
    );
}

const useStylesAddDataHewan = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    paperModal: {
        padding: theme.spacing(2),
        margin: 0,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 0,
        width: '500px',
        height: '400px',
    },
    grid: {
        padding: 0,
        margin: 0,
    }
}));

function submitDataHewan(setOpen, setText) {
    var namaSatwa = window.document.getElementById("namaSatwa").value;
    var jenisSatwa = window.document.getElementById("jenisSatwa").value;
    var tanggalLahir = window.document.getElementById("tanggalLahir").value;
    var tanggalMati = window.document.getElementById("tanggalMati").value;
    if (tanggalMati === "") {
        tanggalMati = false;
    }
    var idKandang = parseInt(window.document.getElementById("idKandang").value);
    // Validasi
    if (Date.parse(tanggalLahir) && idKandang && (!tanggalMati || (tanggalMati && Date.parse(tanggalMati)))) {
        // Valid
        executeKw(2, 'index_react.satwa', 'create', [{
            'nama_satwa': namaSatwa, 'jenis_satwa': jenisSatwa, 'tanggal_lahir_satwa': tanggalLahir, 'tanggal_mati_satwa': tanggalMati, 'id_kandang': idKandang
        }], {}, (r, s, j) => {
            ReactDOM.render(
                <Isi idx={0} variant={1} />,
                document.getElementById('isi')
            );
        }, (j, s, e) => {
            setText(e);
            setOpen(true);
        });
    } else {
        var errText = "";
        if (!Date.parse(tanggalLahir)) {
            errText += "Tanggal Lahir salah (format = YYYY-MM-DD, ";
        }
        if (!idKandang) {
            errText += "Id Kandang salah, ";
        }
        if (tanggalMati && !Date.parse(tanggalMati)) {
            errText += "Tanggal Mati salah (format = YYYY-MM-DD, ";
        }
        setText(errText);
        setOpen(true);
    }
}

function AddDataHewan() {
    const classes = useStylesAddDataHewan();
    const [openModal, setOpen] = React.useState(false);
    const [modalText, setText] = React.useState("");

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <React.Fragment>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openModal}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={openModal}>
                    <Paper className={classes.paperModal}>
                        <Typography>{modalText}</Typography>
                    </Paper>
                </Fade>
            </Modal>
            <Grid item container className={classes.grid} direction="column" spacing={2}>
                <Grid className={classes.grid} item justify="flex-start" container>
                    <Typography variant={'h3'} style={{ color: "#ffffff" }}>Data Hewan</Typography>
                </Grid>
                <Grid className={classes.grid} item justify="center" container>
                    <Paper className={classes.paper}>
                        <Grid className={classes.grid} container justify="center" alignItems="flex-end" spacing={2}>
                            <Grid item xs={4}>
                                <Typography>Nama Satwa</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="namaSatwa" label="Nama Satwa" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Jenis Satwa</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="jenisSatwa" label="Jenis Satwa" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Tanggal Lahir</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="tanggalLahir" label="Tanggal Lahir" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Tanggal Mati</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="tanggalMati" label="Kosongkan jika masih hidup" autoComplete="off" />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Id Kandang</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField id="idKandang" label="Id Kandang" autoComplete="off" />
                            </Grid>

                        </Grid>
                    </Paper>
                </Grid>
                <Grid className={classes.grid} item justify="flex-end" container spacing={2}>
                    <Grid item>
                        <Button variant="contained" className={classes.button} onClick={() => {
                            ReactDOM.render(
                                <Isi idx={0} variant={1} />,
                                document.getElementById('isi')
                            );
                        }}>Cancel</Button>
                    </Grid>
                    <Grid item>
                        <Button variant="contained" className={classes.button} onClick={() => {
                            submitDataHewan(setOpen, setText);
                        }}>Submit</Button>
                    </Grid>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}

window.DataHewan = DataHewan;
window.AddDataHewan = AddDataHewan;