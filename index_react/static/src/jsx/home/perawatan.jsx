const {
    Typography,
    makeStyles,
    Grid,
    Paper,
    Button,
    GridList,
    GridListTile,
    TextField,
    Modal,
    Fade,
    Backdrop
} = MaterialUI;

var cntPerawatan = 0;

function PerawatanListItem({ nama, waktu, caraRawat, cols, ...other }) {
    return (
        <React.Fragment>
            <GridListTile key={cntPerawatan++} cols={cols || 1} {...other} style={{width: '100%'}}>
                <Typography><b>{nama + " " + waktu}</b></Typography>
            </GridListTile>
            {
                caraRawat.map(cr => (
                    <GridListTile key={cntPerawatan++} cols={cols || 1} {...other} style={{width: '100%'}}>
                        <Typography>- {cr}</Typography>
                    </GridListTile>
                ))
            }
        </React.Fragment>
    );
}


const useStylesPerawatan = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        margin: 0,
        width: '500px',
        height: '300px',
    },
    grid: {
        padding: 0,
        margin: 0,
    },
    gridList: {
        width: '500px',
        height: '300px'
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    paperModal: {
        padding: theme.spacing(2),
        margin: 0,
    },
}));

function Perawatan() {
    const classes = useStylesPerawatan();
    const [openModal, setOpen] = React.useState(false);
    const [modalText, setText] = React.useState("");

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <React.Fragment>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openModal}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={openModal}>
                    <Paper className={classes.paperModal}>
                        <Typography>{modalText}</Typography>
                    </Paper>
                </Fade>
            </Modal>
            <Grid item container className={classes.grid} direction="column" spacing={2}>
                <Grid className={classes.grid} item justify="flex-start" container>
                    <Typography variant={'h3'} style={{ color: "#ffffff" }}>Cara Rawat Hewan</Typography>
                </Grid>
                <Grid className={classes.grid} item justify="center" container>
                    <Paper className={classes.paper}>
                        <GridList id="listPerawatan" cellHeight={25} className={classes.gridList} cols={1}/>
                    </Paper>
                </Grid>
                <Grid item justify="flex-end" container spacing={2}>
                    <Grid item>
                        <Button variant="contained" className={classes.button} onClick={() => {
                            ReactDOM.render(
                                <Isi idx={1} variant={2} />,
                                document.getElementById('isi')
                            );
                        }}>Add More</Button>
                    </Grid>
                </Grid>
            </Grid>
            {
                executeKw(uid, 'index_react.perawatan', 'search_read', [], {}, (r, s, j) => {
                    if (r[0].length > 0) {
                        const groupNama = groupBy(r[0], 'nama_satwa');
                        const groupWaktu = groupNama.map(g => groupBy(g, 'waktu_rutin_perawatan'));
                        {/* console.log(groupWaktu); */}
                        const perawatan = []
                        groupWaktu.forEach(n => {
                            {/* console.log(n); */}
                            n.forEach(w => {
                                var temp = [];
                                w.forEach(cara => {
                                    temp.push(cara.cara_perawatan);
                                })
                                {/* console.log(w); */}
                                perawatan.push(<PerawatanListItem nama={w[0].nama_satwa} waktu={w[0].waktu_rutin_perawatan} caraRawat={temp} cols={1}/>)
                            })
                        });
                        {/* console.log(perawatan); */}
                        {/* console.log(<PerawatanListItem nama="D" waktu="08:00" caraRawat={["a", "b"]} cols={1}/>); */}
                        ReactDOM.render(
                            perawatan,
                            document.getElementById('listPerawatan')
                        );
                    }
                }, typicalErrorCB)
            }
        </React.Fragment>
    );
}

const useStylesAddPerawatan = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    paperModal: {
        padding: theme.spacing(2),
        margin: 0,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 0,
        width: '500px',
        height: '400px',
    },
    grid: {
        padding: 0,
        margin: 0,
    },
    gridList: {
        width: '500px',
        height: '220px'
    }
}));

function submitPerawatan(banyakCara) {
    // console.log(banyakCara);
    var namaSatwa = window.document.getElementById("namaSatwa").value;
    var waktu = window.document.getElementById("waktu").value;
    for (let i = 1; i <= banyakCara-1; i++) {
        var cara = window.document.getElementById("cara" + i).value;
        executeKw(2, 'index_react.perawatan', 'create', [{
            'nama_satwa': namaSatwa, 'waktu_rutin_perawatan': waktu, 'cara_perawatan': cara
        }], {}, (r, s, j) => {
            if (i == banyakCara - 1) {
                ReactDOM.render(
                    <Isi idx={1} variant={1} />,
                    document.getElementById('isi')
                );
            }
        }, typicalErrorCB);
    }
}

function CaraListItem({ noCara, keys, cols, ...other }) {
    return (
        <GridListTile key={keys} cols={cols || 1} {...other}>
            <Grid container alignItems="flex-end">
                <Grid item xs={5}>
                    <Typography>{"Cara " + noCara}</Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField id={"cara" + noCara} label={"Cara " + noCara} autoComplete="off" />
                </Grid>
            </Grid>
        </GridListTile>
    );
}

function AddPerawatan() {
    const classes = useStylesAddPerawatan();
    const [banyakCara, setBanyak] = React.useState(1);
    const [cara, setCara] = React.useState([]);
    const addCara = (event, newValue) => {
        setBanyak(banyakCara + 1);
        setCara(cara.concat([{ id: banyakCara }]));
    };
    return (
        <Grid item container className={classes.grid} direction="column" spacing={2}>
            <Grid className={classes.grid} item justify="flex-start" container>
                <Typography variant={'h3'} style={{ color: "#ffffff" }}>Cara Rawat Hewan</Typography>
            </Grid>
            <Grid className={classes.grid} item justify="center" container>
                <Paper className={classes.paper}>
                    <Grid className={classes.grid} container justify="center" alignItems="flex-end" spacing={2}>
                        <Grid item xs={4}>
                            <Typography>Nama Satwa</Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <TextField id="namaSatwa" label="Nama Satwa" autoComplete="off" />
                        </Grid>
                        <Grid item xs={4}>
                            <Typography>Waktu</Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <TextField id="waktu" label="Waktu" autoComplete="off" />
                        </Grid>
                        <Grid item xs={12} container justify="flex-start">
                            <GridList cellHeight={50} cols={1} className={classes.gridList}>
                                {
                                    cara.map(c => (
                                        <CaraListItem noCara={c.id} keys={c.id} />
                                    ))
                                }
                            </GridList>
                        </Grid>
                        <Grid item xs={12} justify="flex-start" container>
                            <Grid item>
                                <Button variant="contained" className={classes.button} onClick={addCara}>+</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
            <Grid className={classes.grid} item justify="flex-end" container spacing={2}>
                <Grid item>
                    <Button variant="contained" className={classes.button} onClick={() => {
                        ReactDOM.render(
                            <Isi idx={1} variant={1} />,
                            document.getElementById('isi')
                        );
                    }}>Cancel</Button>
                </Grid>
                <Grid item>
                    <Button variant="contained" className={classes.button} onClick={() => {
                        submitPerawatan(banyakCara);
                    }}>Submit</Button>
                </Grid>
            </Grid>
        </Grid>
    );
}

window.Perawatan = Perawatan;
window.AddPerawatan = AddPerawatan;