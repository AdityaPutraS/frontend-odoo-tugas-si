// logoImage = '/index_react/static/img/logo.png';
// import logoImage from '/index_react/static/img/logo.png';

const {   
  Grid,
  TextField,
  Button,
  makeStyles,
  CssBaseline,
  Link
} = MaterialUI; 

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: '#87db63',
    height: '100vh',
  },
  appName: {
    color: '#000000',
    fontSize: '5em',
  },
  formPaper: {
    alignSelf: 'center',
    justifySelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  formTitle: {
    color: '#000000',
    marginTop: theme.spacing(2),
  },
  textField: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    width: 200,
  },
  button: {
    margin: theme.spacing(1),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  logo: {
    height: 200,
    width: 200,
    alignSelf: 'center',
    justifySelf: 'center',
    borderRadius: '50%',
  }
}));

function Logo(){
  const classes = useStyles();
  return (
    <img src = {"/index_react/static/img/logo.png"} className={classes.logo} />
  );
}

function validateLogin(){
  $.xmlrpc({
    url: 'http://localhost:8069/xmlrpc/2/common',
    methodName: 'authenticate',
    params: [dbName, username, password, {}],
    success: function(response, status, jqXHR) {
      console.log(response);
      var uid = response - 0;
      $.xmlrpc({
        url: 'http://localhost:8069/xmlrpc/2/object',
        methodName: 'execute_kw',
        params: [
          dbName,
          uid,
          password, 
          'index_react.login', 
          'search_read',
          [],
          {} 
        ],
        success: function(response, status, jqXHR) {
          console.log(response);
          var result = false;
          var loginData = response;
          var username = $('#username')[0].value;
          var password = $('#password')[0].value;
          loginData[0].forEach((element, index) => {
            if(element["username"] == username && element["password"] == password){
              result = true;
              window.document.cookie = "username="+username+"; path=/";
            } 
          });
          if (result){
            console.log("yayy");
            window.location.href = "home";
          }
        },
        error: function(jqXHR, status, error) {console.log(status); console.log(error);}
      });
    },
    error: function(jqXHR, status, error) {console.log(status); console.log(error)}
  });
}

function App() {
  
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Grid container direction= "column" className = {classes.root} justify="center" alignItems="center">
        <Logo />
        <form action="">
          <Grid container direction="column"
            justify="center"
            alignItems="center">
            <TextField className = "loginfield"
              id="username"
              label="Username"
              className={classes.textField}
              margin="normal"
            />
            <TextField className = "loginfield"
              id="password"
              label="Password"
              className={classes.textField}
              type="password"
              margin="normal"
            />
            <Button onClick = {validateLogin} variant="contained" className={classes.button} >
              Login
            </Button>
          </Grid>
        </form>
      </Grid>
    </React.Fragment>
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);