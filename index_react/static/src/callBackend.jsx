function typicalErrorCB(jqXHR, status, error)
{
    console.log("ERROR");
    console.log(jqXHR);
    console.log(status);
    console.log(error);
}

function executeKw(uid, modelName, method, methodParam, executeParam, successCallback, errorCallback) {
    $.xmlrpc({
        url: executeUrl,
        methodName: 'execute_kw',
        params: [
            dbName,
            uid,
            password,
            modelName,
            method,
            methodParam,
            executeParam
        ],
        success: function (response, status, jqXHR) {
            successCallback(response, status, jqXHR);
        },
        error: function (jqXHR, status, error) {
            errorCallback(jqXHR, status, error);
        }
    });
}

function authOdoo(successCallback, errorCallback) {
    $.xmlrpc({
        url: url,
        methodName: 'authenticate',
        params: [dbName, username, password, {}],
        success: function (response, status, jqXHR) {
            successCallback(response, status, jqXHR);
        },
        error: function (jqXHR, status, error) { 
            errorCallback(jqXHR, status, error); 
        }
    });
}

window.typicalErrorCB = typicalErrorCB;
window.executeKw = executeKw;
window.authOdoo = authOdoo;