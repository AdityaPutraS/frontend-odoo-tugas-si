# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.addons.base.models.assetsbundle import AssetsBundle, JavascriptAsset, CompileError
from collections import OrderedDict
from subprocess import Popen, PIPE
from odoo.tools import misc
import logging
_logger = logging.getLogger(__name__)

class ModelLogin(models.Model):
    _name = 'index_react.login'
    username = fields.Char('Username', required=True)
    password = fields.Char('Password', required=True)

class ModelSatwa(models.Model):
    _name = 'index_react.satwa'
    # id_satwa = fields.Integer('ID Satwa', required=True)
    nama_satwa = fields.Char('Nama Satwa', required=True)
    jenis_satwa = fields.Char('Jenis Satwa', required=True)
    tanggal_lahir_satwa = fields.Date('Tanggal Lahir Satwa', required=True)
    tanggal_mati_satwa = fields.Date('Tanggal Mati Satwa')
    id_kandang = fields.Integer('ID Kandang', required=True)

class ModelPakan(models.Model):
    _name = 'index_react.pakan'
    # id_pakan = fields.Integer('ID Pakan', required=True)
    nama_pakan = fields.Char('Nama Pakan', required=True)
    stok_pakan = fields.Integer('Stok Pakan', required=True)

class ModelRekamMedis(models.Model):
    _name = 'index_react.rekam_medis'
    # id_rekam_medis = fields.Integer('ID Rekam Medis', required=True)
    nama_satwa = fields.Char('Nama Satwa', required=True)
    tanggal_rekam_medis = fields.Date('Tanggal Rekam Medis', required=True)
    penyakit = fields.Char('Penyakit', required=True)
    tempat_rawat = fields.Char('Tempat Rawat', required=True)
    dokter = fields.Char('Dokter', required=True)

class ModelKandang(models.Model):
    _name = 'index_react.kandang'
    # id_kandang = fields.Integer('ID Kandang', required=True)
    lokasi_kandang = fields.Char('Lokasi Kandang', required=True)

class ModelPerawatan(models.Model): 
    _name  = 'index_react.perawatan'
    # id_perawatan = fields.Integer('ID Perawatan', required=True)
    nama_satwa = fields.Char('Nama Satwa', required=True)
    cara_perawatan = fields.Char('Cara Perawatan', required=True)
    waktu_rutin_perawatan = fields.Char('Waktu Rutin Perawatan', required=True)

class ModelWaktuMakan(models.Model):
    _name = 'index_react.waktu_makan'
    nama_satwa = fields.Char('Nama Satwa', required=True)
    waktu_makan = fields.Char('Waktu Makan Satwa', required=True)