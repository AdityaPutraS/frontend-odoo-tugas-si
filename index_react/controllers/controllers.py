# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import json

class IndexReactController(http.Controller):

    @http.route('/react/test/', auth='public')
    def test(self, **kw):
        return "WOW"
    @http.route('/react/login/', auth='public')
    def login(self, **kw):
        context = {
            'session_info': json.dumps(request.env['ir.http'].session_info())
        }
        return request.render('index_react.login', qcontext=context)
    @http.route('/react/home/', auth='public')
    def home(self, **kw):
        context = {
            'session_info': json.dumps(request.env['ir.http'].session_info())
        }
        return request.render('index_react.home', qcontext=context)
        # return '''
        # <html>
        #     <body>
        #         <h1>wow</h1>
        #         <p>a</p>
        #     </body>
        # </html>
        # '''